# Didomi CMP

## Remote Config Settings :

GdprType :  3

## Integration Steps

1) **"Install"** or **"Upload"** FG Didomi CMP plugin from the FunGames Integration Manager in Unity, or download it from here.

2) Follow the instructions in the **"Install External Plugin"** section to import Didomi SDK.

3) Click on the **"Prefabs and Settings"** button in the FunGames Integration Manager to fill up your scene with required components and create the Settings asset.

4) To finish your integration, follow the **Account and Settings** section.

## Install External Plugin

After importing the FG GoogleUMP module from the Integration Manager window, you will find the last compatible version of Didomi SDK in the _Assets > FunGames_Externals > GDPR_ folder. Double click on the .unitypackage file to install it.

**Note that the versions of included external SDKs are the latest tested and approved by our development team. We recommend using these versions to avoid any type of discrepencies. If, for any reason, you already have a different version of the plugin installed in your project that you wish to keep, please advise our dev team.**


## Account and Settings

To integrate Didomi CMP throught FunGames SDK you will need to fill the FGDidomiCMPSettings with the Api key we will provide you. Ask your Publisher or Monetization Manager for it.

You will also need to make sure your app is properly added to the list of apps in Didomi dashboard (double check with your Publisher or Monetization Manager).

<img src="_source/didomiCMP.png" width="256" height="540" style="display: block; margin: 0 auto"/>